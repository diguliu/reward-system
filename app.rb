require 'sinatra/base'
require 'sinatra/json'
require './lib/request'

class App < Sinatra::Base
  set :show_exceptions, :after_handler

  post '/' do
    input = params[:file] && params[:file][:tempfile]
    request = Request.new(input)

    request.parse_actions
    request.perform_actions

    json request.scores
  end

  # No input file provided
  error NoMethodError do
    status 422
    json 'No input file'
  end

  # Customer/Action validation failure
  error CustomError do
    status 422
    json env['sinatra.error'].message
  end
end
