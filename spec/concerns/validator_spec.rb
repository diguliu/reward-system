require 'spec_helper'
require './lib/concerns/validator'

RSpec.describe do
  let(:dummy) do
    Class.new do
      attr_accessor :something, :something_else
      include Validator
    end
  end

  subject { dummy.new }

  describe '.validations' do
    it { expect { dummy.validations }.to_not raise_error }
    it { expect(dummy.validations).to eql([]) }
  end

  describe '.validate' do
    let(:validation) { 'something' }
    before { dummy.validate(validation) }
    it { expect(dummy.validations).to include(validation)}
  end

  describe '.validates' do
    let(:v1) { 'something' }
    let(:v2) { 'something_else' }
    let(:validations) { [v1, v2] }
    let(:options) { { presence: true } }

    before { dummy.validates(*validations, options) }

    it { expect(dummy.validations).to include("presence_of_#{v1}")}
    it { expect(dummy.validations).to include("presence_of_#{v2}")}
  end

  describe '#errors' do
    it { expect { subject.errors }.to_not raise_error }
    it { expect(subject.errors).to be_nil }
  end

  describe '#add_error' do
    let(:attr) { :something }
    let(:message) { 'some message' }

    before { subject.add_error(attr, message) }

    it { expect(subject.errors).to have_key(attr) }
    it { expect(subject.errors[attr]).to include(message) }
  end

  describe '#valid?' do
    context 'no validations' do
      it do
        expect(subject.valid?).to be_truthy
        expect(subject.errors).to eql({})
      end
    end

    context 'failed validations' do
      let(:attr) { 'something' }
      let(:validations) { [attr] }
      let(:errors) { {failed: 'some error'} }

      it do
        allow(dummy).to receive(:validations).and_return(validations)
        allow(subject).to receive(:errors).and_return(errors)

        expect(subject.valid?).to be_falsey
      end
    end

    context 'passing validations' do
      let(:attr) { 'something' }
      let(:validations) { [attr] }

      it do
        allow(dummy).to receive(:validations).and_return(validations)
        expect(subject.valid?).to be_truthy
      end
    end
  end

  describe '#verify' do
    it do
      allow(subject).to receive(:valid?).and_return(false)
      expect { subject.verify! }.to raise_error(CustomError)
    end

    it do
      allow(subject).to receive(:valid?).and_return(true)
      expect { subject.verify! }.to_not raise_error
    end
  end

  describe "#validate_presence" do
    let(:attr) { :something }

    it do
      subject.validate_presence(attr)
      expect(subject.errors).to have_key(attr)
      expect(subject.errors[attr]).to include('can not be blank')
    end
  end

  describe "#method_missing" do
    it { expect { subject.not_defined_method }.to raise_error(NoMethodError) }

    it do
      expect(subject).to receive(:validate_presence).with('something')
      expect { subject.presence_of_something }.to_not raise_error
    end
  end
end
