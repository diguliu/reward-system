require 'spec_helper'
require 'rack/test'
require './app'

RSpec.describe App do
  include Rack::Test::Methods

  def app
    App
  end

  let(:parsed_body) { JSON.parse(response.body) }

  context 'without input' do
    let(:response) { post '/' }
    it { expect(response.status).to eq(422) }
    it { expect(parsed_body).to eq('No input file') }
  end

  context 'with invalid input' do
    let(:response) { post '/', file: fixture_file_upload('invalid.txt') }
    it { expect(response.status).to eq(422) }
  end

  context 'self invitation' do
    let(:response) { post '/', file: fixture_file_upload('self-invitation.txt') }
    it { expect(response.status).to eq(422) }
  end

  context 'cyclical invitation' do
    let(:response) { post '/', file: fixture_file_upload('cyclical-invitation.txt') }
    it { expect(response.status).to eq(422) }
  end

  context 'calculate scores for main test case' do
    let(:response) { post '/', file: fixture_file_upload('main.txt') }

    it do
      expect(response.status).to eq(200)
      expect(parsed_body['A']).to eql(1.75)
      expect(parsed_body['B']).to eql(1.5)
      expect(parsed_body['C']).to eql(1.0)
    end
  end
end
