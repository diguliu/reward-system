require 'spec_helper'
require './lib/action'

RSpec.describe Action do
  subject { Action.new(
    source: source,
    target: target,
    verb: verb,
    datetime: datetime
  )}

  let(:source) { double(:source) }
  let(:target) { double(:source) }
  let(:verb) { 'recommends' }
  let(:datetime) { '2019-09-29T13:47:34-03:00' }

  describe 'validations' do
    it { expect(subject.valid?).to be_truthy }

    it 'verifies source presence' do
      subject.source = nil
      expect(subject.valid?).to be_falsey
      expect(subject.errors).to have_key(:source)
    end

    it 'verifies verb presence' do
      subject.verb = nil
      expect(subject.valid?).to be_falsey
      expect(subject.errors).to have_key(:verb)
    end

    it 'verifies datetime presence' do
      subject.datetime = nil
      expect(subject.valid?).to be_falsey
      expect(subject.errors).to have_key(:datetime)
    end

    context 'verb inclusion' do
      it do
        subject.verb = 'recommends'
        expect(subject.valid?).to be_truthy
      end

      it do
        subject.verb = 'accepts'
        subject.target = nil
        expect(subject.valid?).to be_truthy
      end

      it do
        subject.verb = 'something'
        expect(subject.valid?).to be_falsey
        expect(subject.errors).to have_key(:verb)
      end
    end

    context 'verb is recommends' do
      before { allow(subject).to receive(:verb).and_return('recommends') }

      it { expect(subject.valid?).to be_truthy }
      it do
        subject.target = nil
        expect(subject.valid?).to be_falsey
        expect(subject.errors).to have_key(:target)
      end
    end
    
    context 'verb is accepts' do
      before { allow(subject).to receive(:verb).and_return('accepts') }

      it { expect(subject.valid?).to be_falsey }
      it do
        subject.target = nil
        expect(subject.valid?).to be_truthy
      end
    end

    context 'invalid datetime' do
      it do
        subject.datetime = 'abcde'
        expect(subject.valid?).to be_falsey
        expect(subject.errors).to have_key(:datetime)
      end
    end
  end

  describe '#perform' do
    context 'accepts' do
      before { subject.verb = 'accepts' }
      it do
        expect(subject).to receive(:perform_accepts)
        subject.perform
      end
    end

    context 'recommends' do
      before { subject.verb = 'recommends' }
      it do
        expect(subject).to receive(:perform_recommends)
        subject.perform
      end
    end
  end

  describe '#perform_accepts' do
    let(:source) { double(:source)}
    before { allow(subject).to receive(:source).and_return(source) }

    it do
      expect(source).to receive(:accept_invitation)
      subject.perform_accepts
    end
  end

  describe '#perform_recommends' do
    let(:source) { double(:source)}
    let(:target) { double(:target)}

    before do
      allow(subject).to receive(:source).and_return(source)
      allow(subject).to receive(:target).and_return(target)
    end

    it do
      expect(target).to receive(:set_invitation).with(source)
      subject.perform_recommends
    end
  end
end
