require 'spec_helper'
require './lib/custom_error'

RSpec.describe CustomError do
  let(:name) { 'ClassName' }
  let(:klass) { double(:klass, name: name) }
  let(:errors) { {} }
  let(:model) { double(:model, class: klass, errors: errors) }

  subject { described_class.new(model) }

  describe '#initialize' do
    it { expect(subject.model).to eql(model) }
  end

  describe '#message' do
    context 'no errors' do
      it { expect(subject.message).to eql("#{name}: ") }
    end

    context 'one attribute with one error' do
      let(:attr) { :something }
      let(:message) { 'some message' }
      let(:errors) { {attr => [message]} }
      it { expect(subject.message).to eql("#{name}: #{attr} (#{message})") }
    end

    context 'one attribute with multiple errors' do
      let(:attr) { :something }
      let(:m1) { 'some message' }
      let(:m2) { 'some other message' }
      let(:errors) { {attr => [m1, m2]} }
      it { expect(subject.message).to eql("#{name}: #{attr} (#{m1}, #{m2})") }
    end

    context 'multiple attributes with one message' do
      let(:attr1) { :something }
      let(:attr2) { :something_else }
      let(:m1) { 'some message' }
      let(:m2) { 'some other message' }
      let(:errors) { {attr1 => [m1], attr2 => [m2]} }
      it { expect(subject.message).to eql("#{name}: #{attr1} (#{m1}) - #{attr2} (#{m2})") }
    end

    context 'multiple attributes with multiple messages' do
      let(:attr1) { :something }
      let(:attr2) { :something_else }
      let(:m11) { 'some message 1' }
      let(:m12) { 'some other message 1' }
      let(:m21) { 'some message 2' }
      let(:m22) { 'some other message 2' }
      let(:errors) { {attr1 => [m11, m12], attr2 => [m21, m22]} }
      it { expect(subject.message).to eql("#{name}: #{attr1} (#{m11}, #{m12}) - #{attr2} (#{m21}, #{m22})") }
    end
  end
end
