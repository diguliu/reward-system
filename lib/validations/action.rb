require 'date'
require './lib/concerns/validator'

module Validations
  module Action
    def self.included(base)
      base.class_eval do
        include Validator

        validates :source, :datetime, :verb, presence: true
        validate :verb_inclusion
        validate :target_presence
        validate :target_absence
        validate :datetime_format
      end
    end

    def verb_inclusion
      add_error(:verb, 'not allowed') unless %w(recommends accepts).include?(verb)
    end

    def target_presence
      add_error(:target, 'can not be blank with recommends') if verb == 'recommends' && target.nil?
    end

    def target_absence
      add_error(:target, 'must be blank with accepts') if verb == 'accepts' && !target.nil?
    end

    def datetime_format
      begin
        DateTime.parse(datetime.to_s)
      rescue ArgumentError
        add_error(:datetime, 'invalid format')
      end
    end
  end
end
