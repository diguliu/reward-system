require './lib/concerns/validator'

module Validations
  module Customer
    def self.included(base)
      base.class_eval do
        include Validator

        validates :name, presence: true
        validate :self_invitation
        validate :cyclical_invitation
      end
    end

    def self_invitation
      add_error(:invited_by, "can not invite himself") if invited_by == self
    end

    def verify_invitation_loop
      visited = []
      customer = invited_by
      while customer
        return true if visited.include?(customer)
        visited << customer
        customer = customer.invited_by
      end
    end

    def cyclical_invitation
      add_error(:invited_by, "can't form a cyclical dependency") if verify_invitation_loop
    end
  end
end
