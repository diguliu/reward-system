require './lib/customer'
require './lib/action'

class Request
  attr_accessor :input, :actions, :customers

  def initialize(input = nil)
    @input = input.read
    @actions = []
    @customers = {}
  end

  def find_or_create_customer(name)
    customers[name] ||= Customer.new({name: name})
  end

  def parse_actions
    input.strip.split("\n").map do |line|
      date, time, source_name, verb, target_name = line.strip.split(' ')
      source = find_or_create_customer(source_name)
      target = target_name.to_s != '' ? find_or_create_customer(target_name) : nil

      action = Action.new({
        datetime: "#{date} #{time}", 
        source: source,
        verb: verb,
        target: target
      })
      action.verify!

      actions << action
    end
  end

  def perform_actions
    actions.sort_by { |action| DateTime.parse(action.datetime)}.map(&:perform)
  end

  def scores
    customers.values.inject({}) do |result, customer|
      result[customer.name] = customer.score if customer.score > 0
      result
    end
  end
end
