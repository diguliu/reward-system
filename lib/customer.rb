require './lib/validations/customer'

class Customer
  include Validations::Customer

  attr_accessor :name, :score, :invited_by, :accepted_invitation

  def initialize(attributes = {})
    @name = attributes[:name] || ''
    @score = attributes[:score] || 0.0
    @invited_by = attributes[:invited_by]
    @accepted_invitation = attributes[:accepted_invitation] || false
  end

  MODIFIER = 2.0

  def award(value)
    value = value / MODIFIER
    self.score += value

    invited_by.award(value) if invited_by
  end

  def set_invitation(inviter)
    self.invited_by ||= inviter
    self.verify! # Ensure no self or cyclical invitation was made
  end

  def accept_invitation
    if !accepted_invitation && invited_by
      self.accepted_invitation = true

      # Setting the award to MODIFIER as initial value is a trick to ensure
      # it'll become 1 on the first level and also work conveniently on further
      # levels.
      invited_by.award(MODIFIER)
    end
  end
end
