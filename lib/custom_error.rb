class CustomError < StandardError
  attr_reader :model

  def initialize(model = nil)
    @model = model
  end

  def message
    result = "#{model.class.name}: "
    result += model.errors.map do |attr, errors|
      "#{attr} (#{errors.join(', ')})"
    end.join(' - ')

    result
  end
end
