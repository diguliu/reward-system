require './lib/validations/action'

class Action
  include Validations::Action

  attr_accessor :source, :target, :verb, :datetime, :processed

  def initialize(attributes = {})
    @source = attributes[:source]
    @target = attributes[:target]
    @verb = attributes[:verb]
    @datetime = attributes[:datetime]
    @processed = attributes[:processed] || false
  end

  def perform_accepts
    source.accept_invitation
  end

  def perform_recommends
    target.set_invitation(source)
  end

  def perform
    self.send("perform_#{verb}")
  end
end
