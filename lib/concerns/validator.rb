require './lib/custom_error'

module Validator
  def self.included(base)
    base.extend ClassMethods
    base.class_eval do
      attr_accessor :errors
      @validations = []
    end
  end

  module ClassMethods
    attr_reader :validations

    def validate(validation)
      @validations << validation
    end

    def validates(*validations, options)
      if options[:presence]
        @validations += validations.map { |v| "presence_of_#{v}" }
      end
    end
  end

  def add_error(attr, message)
    attr = attr.to_sym
    self.errors ||= {}
    self.errors[attr] ||= []
    self.errors[attr] << message
  end

  def valid?
    self.errors = {}
    self.class.validations.each do |validation|
      send(validation)
    end

    errors.empty?
  end

  def verify!
    if !valid?
      raise CustomError.new(self)
    end
  end

  def validate_presence(attr)
    add_error(attr, "can not be blank") if send(attr).to_s.empty?
  end

  def method_missing(method, *args, &block)
    result = method.to_s.match(/^presence_of_(.+)$/)
    if result
      validate_presence(result[1])
    else
      super
    end
  end
end
