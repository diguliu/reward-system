# Reward System

A simple application that calculates invitations' rewards when provided with series of actions.

## Solution

This was a simple but very insteresting problem that has some principles that
we can find on real world applications. This was made using Sinatra and without
any database persistence.

I was able to build the application around 3 basic models:
1. Request
1. Action
1. Customer

### Request

The Request is a higher level entity that scopes Customers and Actions. A new
Request is created for each access to the endpoint. It's responsible for accessing
the original input file, parsing it into proper customers and actions,
performing actions and displaying customers scores.

### Action

The Action is a model that represent each line of the input. Using this
structure I was able to conveniently use the action's validation to
validate the input format and ensure that every action follows the proper
requirements. A downside of this approach is that it doesn't provide a simple
way display exactly where each error occurred on each line. This structure
would need to be reconsidered if this was a strong requirement of the
application.

Besides all the validation, the Action also triggers the proper actions on
Customer being it and "accepts" or a "recommends" action.

### Customer

The Customer model handles most of the effective business logic of the
application. Defining the "invited\_by" relation (with Customer itself) we
control who invited who, we make sure that one someone is invited the
"invited\_by" can't be overwritten by another invitation and also validate that
there is no self-invitation or cyclical invitation (A invites B that invites A).

To calculate the scores, we perform a chain "award" method that start from the
acceptance of an invitation. When the invitation is accepted the one that send
it receives the full reward and we keep rewarding everyone in the invitation
chain dividing the initial reward (1) by 2 (MODIFIER) on each step until we
find a customer that was not invited by anyone. This process is loop-safe
because we validate customers to avoid self and cyclical invitation.
Effectively the scores are incrementally calculated on each "accept" action
that is performed.

## Getting Started

These instructions will get you a copy of the project up and running on your
local machine for development and testing purposes. See deployment for notes on
how to deploy the project on a live system.

### Prerequisites

*  Bundler

### Installing

Install gem dependencies by running:

```
bundle
```

Start the application:

```
bundle exec thin start
```

You should see something like this:

```
Using rack adapter
Thin web server (v1.7.2 codename Bachmanity)
Maximum connections set to 1024
Listening on 0.0.0.0:3000, CTRL+C to stop
```

Voilá!

## Running the tests

To run the tests use:

```
bundle exec rspec
```

## Live testing

A live demo of the application is available at: https://diguliu-reward-system.herokuapp.com/

You can live test it using a api testing tool like [Postman](https://www.getpostman.com/).

The application has only one endpoint that receives a plain text file containing basic actions and returning a json with each customer's score:
*  **POST** / (file: plain text) 

The file should be structured based on the following example:
```
2018-06-12 09:41 A recommends B
2018-06-14 09:41 B accepts
2018-06-16 09:41 B recommends C
2018-06-17 09:41 C accepts
2018-06-19 09:41 C recommends D
2018-06-23 09:41 B recommends D
2018-06-25 09:41 D accepts
```

## Deployment

Being a simple sinatra application you should be able to deploy it easily
almost anywhere. You can deploy to Heroku for example using `dpl`. Just install
it first:

```
gem install dpl

```

An then run:

```
dpl --provider=heroku --app=YOUR-APP-NAME --api-key=YOUR-APP-KEY
```

Look for further documentation at: https://github.com/travis-ci/dpl

## Built With

* [Sinatra](http://sinatrarb.com/) - Web framework
* [Bundler](https://bundler.io/) - Dependency management

## Authors

* **Rodrigo Souto** - [Gitlab](https://gitlab.com/diguliu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
